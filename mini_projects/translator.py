from translate import Translator
from tkinter import *

window = Tk()
window.title("Translator")
window.config(bg='')
window.geometry("500x300")

title_label = Label(window,
                    text="Translator",
                    font=("Bell MT", 35),
                    fg='blue')

# to put into window
title_label.pack(padx=(30, 0))

language_1 = StringVar()
language_2 = StringVar()

language_list = ['English', 'Italian', 'Polish']

language_1.set('English')
language_2.set('Polish')

language_1_menu = OptionMenu(window, language_1, *language_list)
language_1_menu.config(font=('Helvetica', 20))
language_1_menu.pack(side=LEFT, padx=(130, 0), pady=(0,150))


language_2_menu = OptionMenu(window, language_2, *language_list)
language_2_menu.config(font=('Helvetica', 20))
language_2_menu.pack(side=RIGHT, padx=(0, 130), pady=(0,150))

# infinite loop
mainloop()
