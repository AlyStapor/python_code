

letters = ['a','b','g','w','i','s','e']


def filter_Vowel(letter):
    vowels = ['a','e','o','i','u']
    if letter in vowels:
        return True
    else:
        return False


filtered_vowels = filter(filter_Vowel, letters)
# filter is taking each value from list and passing it into function

for vowel in filtered_vowels:
    print(vowel)


numbers = [2, 46, 6, 43, 32, 11, 214, 45]
filtered_evens = filter(lambda x: x%2==0, numbers)
print(list(filtered_evens))