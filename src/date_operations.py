import datetime

# days, months, year
d = datetime.date(2021,12,5)
# print(d)

t = datetime.date.today()
# print(t.month)
# print(t.weekday())
# Monday 0, Sunday 6
# print(t.isoweekday())
# Monday 1 Sunday 7

t_delta = datetime.timedelta(days=12)
# print(t + t_delta)

new_date = t - t_delta
# print(new_date)

# how many days left till some date
my_bday = datetime.date(2022,5,5)
days_left = my_bday - t
# print(f"Days till my next birthday: {days_left}")

# hours, minutes, seconds
time = datetime.time(8,32,11)
print(time)

# combination of both
time = datetime.datetime(2021, 8, 2, 12, 32, 30)
print(time)

time_delta = datetime.timedelta(days=25)
print(time + time_delta)

# not possible to add timezone:
print(f".today: {datetime.datetime.today()}")
# here we can add timezone:
print(f".now: {datetime.datetime.now()}")
print(f".utcnow: {datetime.datetime.utcnow()}")