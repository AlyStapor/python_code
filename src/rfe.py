from sklearn.model_selection import train_test_split
from sklearn.preprocessing import (StandardScaler,
                                   MinMaxScaler)
from sklearn.ensemble import RandomForestClassifier


def run_rfe(df, X, target, n_features):
    X_train, X_test, Y_train, Y_test = train_test_split(X = X,
                                                        Y = target,
                                                        test_size=0.2,
                                                        random_state=42)
    scaler = StandardScaler().fit(X_train)
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)


    model = RandomForestClassifier(citerion='entropy', random_state=42)
    rfe = RFE(model, n_features)
    rfe = rfe.fit(X_train_scaled, y_train)

    feature_names = df.columns[rfe.get_support()]
    return feature_names

