
# returns a single value

import functools

numbers = [1,3,4,5,6,7,8,100]

print(functools.reduce(lambda a,b: a+b, numbers))

print(functools.reduce(lambda a,b: a if a>b else b, numbers))
