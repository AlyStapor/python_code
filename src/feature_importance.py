import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier


def get_feature_importance(number_of_features,
                           X, target):
    X_train, X_test, Y_train, Y_test = train_test_split(X = X,
                                                        Y = target,
                                                        test_size=0.2,
                                                        stratify=Y,
                                                        random_state=42)

    model = RandomForestClassifier()
    model = model.fit(X_train, Y_train)

    feat_importance = pd.Series(model.feature_importances_, index = X.columns)
    feat_importance.nlargesr(number_of_features).plot(kind='barh')
    plt.show()

    return model


def select_features(model, df, threshold):
    model = SelectModel(model = model, prefit=True, threshold=threshold)
    feature_idx = model.get_support()
    feature_names = df.columns[feature_idx]
    return feature_names
