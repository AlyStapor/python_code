import pandas as pd
import numpy as np

def check_args(df, key: str, value):
    if not value:
        return df
    return df[df[key] == value]


def filter_df(df: pd.DataFrame,
              additional_args: dict):
    additional_args = additional_args or {}
    for column, value in additional_args.items():
        df = check_args(df, column, value)

    return df


# group by two columns and makes operations only on subsets

def get_some_value(val):

    def makes_operation(val):
        val.reset_index(inplace=True) # or whatever operation needed
        min_date = val['date'].min() # whatever

        # operations per row
        for i in range(1, len(val)):
            val.loc[i, 'column A'] = val.loc[i - 1, 'column B'] + val.loc[i, 'column C'] - \
                                                             val.loc[i, 'column A']

        return val

    return val.groupby('Column to groupby').apply(makes_operation)


test = df.groupby('Column to groupby').apply(get_some_value)