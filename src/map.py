
# traditional
def square(num):
    return num*num


list_num = (1,2,3,4,5)

# map
result = map(square, list_num)
print(list(result))

# with lambda
result_l = map(lambda x: x*x, list_num)
print(list(result_l))

# we can have as many list as we want
list_num_2 = (5,6,7,8,9)
result_m = map(lambda x, y: x+y, list_num, list_num_2)
print(list(result_m))


# makes lists
list1 = ['mon', 'tues', 'wed', 'thurs', 'fri']
result = list(map(list, list1))
print(result)