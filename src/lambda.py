
# no return as in traditional functions
a=lambda x,y,z : x*y-7*z+14
print(a(5,3,2))

# which is bigger value
max=lambda x, y: x if x > y else y

print(max(5,7))


def func(num):
    return lambda x: x*num


# this return lambda function
a = func(10)

# this pass value to the lambda function
print(a(5))

# with traditional functions we have to write more line of code
