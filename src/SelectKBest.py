from sklearn.model_selection import train_test_split
from sklearn.preprocessing import (StandardScaler,
                                   MinMaxScaler)
from sklearn.feature_selection import SelectKBest


def univatiate_selection(X, target, df):
    X_train, X_test, Y_train, Y_test = train_test_split(X = X,
                                                        Y = target,
                                                        test_size = 0.2,
                                                        stratify=Y,
                                                        random_state=42)
    scaler = StandardScaler().fit(X_train)
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    min_max_scaler = MinMaxScaler()
    scaled_X = min_max_scaler.fit_transform(X_train_scaled)

    selector = SelectKBest(chi2, k=20)
    X_new = selector.fit_transform(scaled_X, Y_train)
    feature_idx = selector.get_support()
    feature_names = df.columns[feature_idx]
    return feature_names

