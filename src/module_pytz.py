import datetime
import pytz

date = datetime.datetime(2021, 12, 6, 22, 30, 59, tzinfo=pytz.UTC)
print(date)

date_now = datetime.datetime.now(tz=pytz.UTC)
print(date_now)

date_here = date_now.astimezone(pytz.timezone('Europe/Warsaw'))
print(date_here)

# all timezones:
# for tz in pytz.all_timezone:
#    print(tz)

# changing format
print(date_here.isoformat())
# any format of your choice:
print(date_here.strftime('%d %B %Y'))

# string to datetime
date_str = "5 May 1996"
dt = datetime.datetime.strptime(date_str, '%d %B %Y')
print(dt)